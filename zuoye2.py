import time
#from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By


def test_search():
    driver= webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(4)

    # 打开测试人网站
    driver.get("https://ceshiren.com")


    # 选中“精华帖”


    driver.find_element(By.CSS_SELECTOR, ".nav-item_精华帖").click()

    # 点击放大镜按钮
    time.sleep(5)
    driver.find_element(by=By.ID,value='search-button').click()


    # 输入搜索关键词
    driver.find_element(by=By.ID,value='search-term').send_keys('python')
    #driver.find_element(by=By.ID,value='search-button').send_keys('python')


    # 在搜索结果中点击“精华帖”
    driver.find_element(By.XPATH,'//*[@class="search-menu-assistant-item"]//*[@href="/tag/精华帖"]').click()


    # 预期：第三条是xx
    eles = driver.find_elements(By.CLASS_NAME, "topic-title")
    assert "某物业项目测试方案"  == eles[3].text